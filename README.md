**Installation**

    1) Clone
    https://r_sriganeshraja@bitbucket.org/r_sriganeshraja/creadi.git
    
    2) NPM Install
    npm install

**Run**

    1) Compile Code
    gulp compile
    
    2) Run Application
    npm start -- --example="app"
    
**Routes**

    **User/Auth Routes**
    
    1) User Registration
    
    `curl -X POST \
      http://creadi.local:3000/auth/register \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/x-www-form-urlencoded' \
      -d 'email=r.sriganeshraja%40web-creaters.org&password=testing123456&name=Sriganeshraja Ramalingam'`
      
    2) Authentication`
    
    `curl -X POST \
      http://creadi.local:3000/auth/authenticate \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/x-www-form-urlencoded' \
      -d 'email=r.sriganeshraja%40web-creaters.org&password=testing123456'`
      
    3) User Detail Show
    
    `curl -X GET \
       http://creadi.local:3000/user/1 \
       -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InIuc3JpZ2FuZXNocmFqYUB3ZWItY3JlYXRlcnMub3JnIiwiaWF0IjoxNDkyNDE4MzgyLCJleHAiOjE0OTI1MDQ3ODJ9.YC0Kj2J7jlRIfDJD8_tcPGAnDQRAml95XWjQeD7SPWE' \
       -H 'cache-control: no-cache'`
       
    4) User Details Update
    
    `curl -X POST \
       http://creadi.local:3000/user/1 \
       -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InIuc3JpZ2FuZXNocmFqYUB3ZWItY3JlYXRlcnMub3JnIiwiaWF0IjoxNDkyNDE4MzgyLCJleHAiOjE0OTI1MDQ3ODJ9.YC0Kj2J7jlRIfDJD8_tcPGAnDQRAml95XWjQeD7SPWE' \
       -H 'cache-control: no-cache' \
       -d 'email=r.sriganeshraja111%40web-creaters.org&password=testing123456'`
       
    **Contract Routes**
    
    1) List all Contract
    
    `curl -X GET \
       http://creadi.local:3000/contract/ \
       -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InIuc3JpZ2FuZXNocmFqYUB3ZWItY3JlYXRlcnMub3JnIiwiaWF0IjoxNDkyNDE4MzgyLCJleHAiOjE0OTI1MDQ3ODJ9.YC0Kj2J7jlRIfDJD8_tcPGAnDQRAml95XWjQeD7SPWE' \
       -H 'cache-control: no-cache'`
    
    2) Add Contract
    
    `curl -X POST \
       http://creadi.local:3000/contract/ \
       -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InIuc3JpZ2FuZXNocmFqYUB3ZWItY3JlYXRlcnMub3JnIiwiaWF0IjoxNDkyNDE4MzgyLCJleHAiOjE0OTI1MDQ3ODJ9.YC0Kj2J7jlRIfDJD8_tcPGAnDQRAml95XWjQeD7SPWE' \
       -H 'cache-control: no-cache' \
       -H 'content-type: application/x-www-form-urlencoded' \
       -d 'name=Contract%204&description=Contract%204%20full%20Details'`
    
    3) Update Contract
    
    `curl -X POST \
       http://creadi.local:3000/contract/4 \
       -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InIuc3JpZ2FuZXNocmFqYUB3ZWItY3JlYXRlcnMub3JnIiwiaWF0IjoxNDkyNDE4MzgyLCJleHAiOjE0OTI1MDQ3ODJ9.YC0Kj2J7jlRIfDJD8_tcPGAnDQRAml95XWjQeD7SPWE' \
       -H 'cache-control: no-cache' \
       -H 'content-type: application/x-www-form-urlencoded' \
       -d 'name=Contract%204&description=Contract%204%20full%20Details'`
    
    4) Delete Contract
    
    `curl -X DELETE \
       http://creadi.local:3000/contract/1 \
       -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InIuc3JpZ2FuZXNocmFqYUB3ZWItY3JlYXRlcnMub3JnIiwiaWF0IjoxNDkyNDE4MzgyLCJleHAiOjE0OTI1MDQ3ODJ9.YC0Kj2J7jlRIfDJD8_tcPGAnDQRAml95XWjQeD7SPWE' \
       -H 'cache-control: no-cache'`
    