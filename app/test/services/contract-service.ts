/* tslint:disable:no-require-imports */

import {expect} from "chai";
import {sequelize} from "../../src/models/index";
import {ContractInstance} from "../../src/models/interfaces/contract-interface";
import {ContractService} from "../../src/services/contract-service";

const delay = 500;

describe("ContractService", () => {
  const contractAttributes = {
    id: 101,
    name: "Contract 101",
    description: "Contract 101 Full Description"
  };
  const updateContractAttributes = {
    name: "Contract 102",
    description: "Contract 102 Full Description"
  };
  describe("#Contract Actions", () => {
    let contractService: ContractService;

    before((done: Function) => {
      setTimeout(() => {
        sequelize.sync().then(() => {
          let service = require("../../src/services/contract-service");
          contractService = service.contractService;
          done();
        }).catch((error: Error) => {
          done(error);
        });
      }, delay);
    });

    it("should create a contract in the database correctly", () => {
      contractService.createContract(contractAttributes).then((contract: ContractInstance) => {
        expect(contract.dataValues.name).to.equals(contractAttributes.name);
        expect(contract.dataValues.description).to.equals(contractAttributes.description);
      }).catch((error: Error) => {
        console.log(error.message);
      });
    });

    it("should retrieve a contract from the database correctly", () => {
      contractService.retrieveContract(contractAttributes.id).then((contract: ContractInstance) => {
        expect(contract.dataValues.name).to.equals(contractAttributes.name);
        expect(contract.dataValues.description).to.equals(contractAttributes.description);
      }).catch((error: Error) => {
        console.log(error.message);
      });
    });

    it("should update a contract from the database correctly", () => {
      contractService.updateContract(contractAttributes.id, updateContractAttributes).then((result) => {
        expect(result).to.be.a('null');
      }).catch((error: Error) => {
        console.log(error.message);
      });
    });

    it("should delete a contract from the database correctly", () => {
      contractService.deleteContract(contractAttributes.id).then((result) => {
        expect(result).to.be.a('null');
      }).catch((error: Error) => {
        console.log(error.message);
      });
    });
  });
});
