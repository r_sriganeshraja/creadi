/* tslint:disable:no-require-imports */

import {expect} from "chai";
import {sequelize} from "../../src/models/index";
import {UserInstance} from "../../src/models/interfaces/user-interface";
import {UserService} from "../../src/services/user-service";

const delay = 500;

describe("UserService", () => {
  const newUserAttributes = {
    id: 101,
    name: "User 1",
    email: "email1@email.com",
    password: "password123456"
  };
  const updateUserAttributes = {
    name: "User 2",
    email: "email2@email.com",
    password: "password123456"
  };

  describe("#User Actions", () => {
    let userService: UserService;

    before((done: Function) => {
      setTimeout(() => {
        sequelize.sync().then(() => {
          let service = require("../../src/services/user-service");
          userService = service.userService;
          done();
        }).catch((error: Error) => {
          done(error);
        });
      }, delay);
    });

    it("should create a user in the database correctly", () => {
      userService.createUser(newUserAttributes).then((user: UserInstance) => {
        expect(user.dataValues.name).to.equals(newUserAttributes.name);
        expect(user.dataValues.email).to.equals(newUserAttributes.email);
        expect(user.dataValues.password).to.equals(newUserAttributes.password);
      }).catch((error: Error) => {
        console.log(error.message);
      });
    });

    it("should retrieve a user from the database correctly", () => {
      userService.retrieveUser(newUserAttributes.id).then((user: UserInstance) => {
        expect(user.dataValues.email).to.equals(newUserAttributes.email);
        expect(user.dataValues.name).to.equals(newUserAttributes.name);
      }).catch((error: Error) => {
        console.log(error.message);
      });
    });

    it("should update a user from the database correctly", () => {
      userService.updateUser(newUserAttributes.id, updateUserAttributes).then((result) => {
        expect(result).to.be.a('null');
      }).catch((error: Error) => {
        console.log(error.message);
      });
    });
  });
});
