import {Instance} from "sequelize";

export interface UserAttributes {
  id: number;
  email: string;
  name: string;
  password: string;
}

export interface UserPublicAttributes {
  id: number;
  name: string;
}

export interface UserInstance extends Instance<UserAttributes> {
  dataValues: UserAttributes;
}

export interface UserPublicInstance extends Instance<UserPublicAttributes> {
  dataValues: UserPublicAttributes;
}
