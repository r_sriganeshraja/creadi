import {Instance} from "sequelize";

export interface ContractAttributes {
  id: number;
  name: string;
  description: string;
}

export interface ContractInstance extends Instance<ContractAttributes> {
  dataValues: ContractAttributes;
}
