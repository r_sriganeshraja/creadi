/* tslint:disable:variable-name */

import * as SequelizeStatic from "sequelize";
import {DataTypes, Sequelize} from "sequelize";
import {ContractAttributes, ContractInstance} from "./interfaces/contract-interface";

export default function(sequelize: Sequelize, dataTypes: DataTypes):
  SequelizeStatic.Model<ContractInstance, ContractAttributes> {
  let Contract = sequelize.define<ContractInstance, ContractAttributes>("Contract", {
    id: {type: dataTypes.INTEGER.UNSIGNED, allowNull: false, primaryKey: true, autoIncrement: true},
    name: {type: dataTypes.STRING, allowNull: false, unique: true},
    description: {type: dataTypes.TEXT, allowNull: true}
  }, {
    indexes: [],
    classMethods: {},
    timestamps: true
  });

  return Contract;
}
