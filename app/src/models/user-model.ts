/* tslint:disable:variable-name */

import * as SequelizeStatic from "sequelize";
import {DataTypes, Sequelize} from "sequelize";
import {UserAttributes, UserInstance} from "./interfaces/user-interface";

export default function(sequelize: Sequelize, dataTypes: DataTypes):
  SequelizeStatic.Model<UserInstance, UserAttributes> {
  let User = sequelize.define<UserInstance, UserAttributes>("User", {
    id: {type: dataTypes.INTEGER.UNSIGNED, allowNull: false, primaryKey: true, autoIncrement: true},
    email: {type: dataTypes.STRING, allowNull: false, unique: true},
    name: {type: dataTypes.STRING, allowNull: true},
    password: {type: dataTypes.STRING, allowNull: false}
  }, {
    indexes: [],
    classMethods: {},
    timestamps: true
  });

  return User;
}
