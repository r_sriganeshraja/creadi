import {logger} from "../utils/logger";
import {models, sequelize} from "../models/index";
import {UserAttributes, UserInstance, UserPublicAttributes, UserPublicInstance} from "../models/interfaces/user-interface";
import {Transaction} from "sequelize";

export class UserService {
  createUser(userAttributes: UserAttributes): Promise<UserInstance> {
    let promise = new Promise<UserInstance>((resolve: Function, reject: Function) => {
      sequelize.transaction((t: Transaction) => {
        return models.User.create(userAttributes).then((user: UserInstance) => {
          logger.info('Created user with name ${userAttributes.name}.');
          resolve(user);
        }).catch((error: Error) => {
          if(error.name === "SequelizeUniqueConstraintError") {
            error = new Error(`User with '${userAttributes.name}' is already exists`);
            logger.error('User with `${userAttributes.name}` is already exists');
          } else {
            logger.error(error.message);
          }
          reject(error);
        });
      });
    });

    return promise;
  }

  authenticateUser(email: string, password: string): Promise<UserInstance> {
    let promise = new Promise<UserInstance>((resolve: Function, reject: Function) => {
      sequelize.transaction((t: Transaction) => {
        return models.User.findOne({where: {email: email, password: password}}).then((user: UserInstance) => {
          if (user) {
            logger.info(`Retrieved user with EMail ID ${email}.`);
            resolve(user);
          } else {
            logger.info(`User with EMail ID ${email} does not exist.`);
            var error = new Error("Check Username/Password");
            reject(error);
          }
        }).catch((error: Error) => {
          logger.error(error.message);
          reject(error);
        });
      });
    });

    return promise;
  }

  retrieveUser(id: number): Promise<UserPublicInstance> {
    let promise = new Promise<UserPublicInstance>((resolve: Function, reject: Function) => {
      sequelize.transaction((t: Transaction) => {
        return models.User.findOne({where: {id: id}}).then((user: UserPublicInstance) => {
          if (user) {
            logger.info(`Retrieved user with id ${id}.`);
          } else {
            logger.info(`User with id ${id} does not exist.`);
          }
          resolve(user);
        }).catch((error: Error) => {
          logger.error(error.message);
          reject(error);
        });
      });
    });

    return promise;
  }

  updateUser(id: number, userAttributes: any): Promise<void> {
    let promise = new Promise<void>((resolve: Function, reject: Function) => {
      sequelize.transaction((t: Transaction) => {
        return models.User.update(userAttributes, {where: {id: id}})
          .then((results: [number, Array<UserInstance>]) => {
          if (results.length > 0) {
            logger.info(`Updated user with id ${id}.`);
          } else {
            logger.info(`User with id ${id} does not exist.`);
          }
          resolve(null);
        }).catch((error: Error) => {
          logger.error(error.message);
          reject(error);
        });
      });
    });

    return promise;
  }
}

export const userService = new UserService();
