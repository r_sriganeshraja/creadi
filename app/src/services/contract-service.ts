import {logger} from "../utils/logger";
import {models, sequelize} from "../models/index";
import {ContractAttributes, ContractInstance} from "../models/interfaces/contract-interface";
import {Transaction} from "sequelize";

export class ContractService {
  createContract(contractAttributes: ContractAttributes): Promise<ContractInstance> {
    let promise = new Promise<ContractInstance>((resolve: Function, reject: Function) => {
      sequelize.transaction((t: Transaction) => {
        return models.Contract.create(contractAttributes).then((contract: ContractInstance) => {
          logger.info(`Created contract with id ${contractAttributes.id}.`);
          resolve(contract);
        }).catch((error: Error) => {
          logger.error(error.message);
          reject(error);
        });
      });
    });

    return promise;
  }

  retrieveContract(id: number): Promise<ContractInstance> {
    let promise = new Promise<ContractInstance>((resolve: Function, reject: Function) => {
      sequelize.transaction((t: Transaction) => {
        return models.Contract.findOne({where: {id: id}}).then((contract: ContractInstance) => {
          if (contract) {
            logger.info(`Retrieved contract with id ${id}.`);
          } else {
            logger.info(`Contract with id ${id} does not exist.`);
          }
          resolve(contract);
        }).catch((error: Error) => {
          logger.error(error.message);
          reject(error);
        });
      });
    });

    return promise;
  }

  retrieveContracts(): Promise<Array<ContractInstance>> {
    let promise = new Promise<Array<ContractInstance>>((resolve: Function, reject: Function) => {
      sequelize.transaction((t: Transaction) => {
        return models.Contract.findAll().then((contracts: Array<ContractInstance>) => {
          logger.info("Retrieved all contracts.");
          resolve(contracts);
        }).catch((error: Error) => {
          logger.error(error.message);
          reject(error);
        });
      });
    });

    return promise;
  }

  updateContract(id: number, contractAttributes: any): Promise<void> {
    let promise = new Promise<void>((resolve: Function, reject: Function) => {
      sequelize.transaction((t: Transaction) => {
        return models.Contract.update(contractAttributes, {where: {id: id}})
          .then((results: [number, Array<ContractInstance>]) => {
          if (results.length > 0) {
            logger.info(`Updated contract with id ${id}.`);
          } else {
            logger.info(`Contract with id ${id} does not exist.`);
          }
          resolve(null);
        }).catch((error: Error) => {
          logger.error(error.message);
          reject(error);
        });
      });
    });

    return promise;
  }

  deleteContract(id: number): Promise<void> {
    let promise = new Promise<void>((resolve: Function, reject: Function) => {
      sequelize.transaction((t: Transaction) => {
        return models.Contract.destroy({where: {id: id}}).then((afffectedRows: number) => {
          if (afffectedRows > 0) {
            logger.info(`Deleted contract with id ${id}`);
          } else {
            logger.info(`Contract with id ${id} does not exist.`);
          }
          resolve(null);
        }).catch((error: Error) => {
          logger.error(error.message);
          reject(error);
        });
      });
    });

    return promise;
  }
}

export const contractService = new ContractService();
