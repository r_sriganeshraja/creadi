import {contractService} from "../services/contract-service";
import {ContractInstance} from "../models/interfaces/contract-interface";
import {Request, Response, Router} from "express";

export const router = Router();

router.post("/", (req: Request, res: Response) => {
  contractService.createContract(req.body).then((contract: ContractInstance) => {
    return res.status(201).send(contract);
  }).catch((error: Error) => {
    return res.status(409).send(error);
  });
});

router.get("/:id", (req: Request, res: Response) => {
  contractService.retrieveContract(req.params.id).then((contract: ContractInstance) => {
    if (contract) {
      return res.send(contract);
    } else {
      return res.sendStatus(404);
    }
  }).catch((error: Error) => {
    return res.status(500).send(error);
  });
});

router.get("/", (req: Request, res: Response) => {
  contractService.retrieveContracts().then((contracts: Array<ContractInstance>) => {
    return res.send(contracts);
  }).catch((error: Error) => {
    return res.status(500).send(error);
  });
});

router.post("/:id", (req: Request, res: Response) => {
  contractService.updateContract(req.params.id, req.body).then(() => {
    return res.sendStatus(200);
  }).catch((error: Error) => {
    return res.status(409).send(error);
  });
});

router.delete("/:id", (req: Request, res: Response) => {
  contractService.deleteContract(req.params.id).then(() => {
    return res.sendStatus(200);
  }).catch((error: Error) => {
    return res.status(500).send(error);
  });
});
