import {userService} from "../services/user-service";
import {UserInstance} from "../models/interfaces/user-interface";
import {Request, Response, Router} from "express";
import {configs} from "../../../config/config";

const jwt = require("jsonwebtoken");

export const router = Router();

router.post("/register", (req: Request, res: Response) => {
  userService.createUser(req.body).then((user: UserInstance) => {
    return res.status(201).send(user);
  }).catch((error: Error) => {
    return res.status(409).send(error);
  });
});

router.post("/authenticate", (req: Request, res: Response) => {
    userService.authenticateUser(req.body.email, req.body.password).then((user: UserInstance) => {
        var token = jwt.sign({'email': req.body.email}, configs.getServerConfig().superSecret, {
            expiresIn: 86400
        });

        return res.json({
            success: true,
            message: 'Enjoy your token!',
            token: token
        });
    }).catch((error: Error) => {
        return res.status(401).json({
            success: false,
            message: error.message,
            token: null
        });
    });
});