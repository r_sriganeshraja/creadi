import {userService} from "../services/user-service";
import {UserInstance, UserPublicInstance} from "../models/interfaces/user-interface";
import {Request, Response, Router} from "express";
import {configs} from "../../../config/config";

export const router = Router();

router.get("/:id", (req: Request, res: Response) => {
  userService.retrieveUser(req.params.id).then((user: UserPublicInstance) => {
    if (user) {
      return res.send(user);
    } else {
      return res.sendStatus(404);
    }
  }).catch((error: Error) => {
    return res.status(500).send(error);
  });
});

router.post("/:id", (req: Request, res: Response) => {
  userService.updateUser(req.params.id, req.body).then(() => {
    return res.sendStatus(200);
  }).catch((error: Error) => {
    return res.status(409).send(error);
  });
});
