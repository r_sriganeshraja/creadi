export interface DatabaseConfig {
    username: string;
    password: string;
    database: string;
    host: string;
    port: number;
    dialect: string;
    logging: boolean | Function;
    force: boolean;
    timezone: string;
}

export const databaseConfig: DatabaseConfig = {
    username: "creadi",
    password: "creadi",
    database: "creadi",
    host: "127.0.0.1",
    port: 3306,
    dialect: "mysql",
    logging: true,
    force: false,
    timezone: "+01:00"
};