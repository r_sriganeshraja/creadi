export interface ServerConfig {
    port: number;
    superSecret: string;
    session: {
        secret: string,
        name: string,
        resave: boolean,
        saveUninitialized: boolean,
        proxy: boolean
    };
}

export const serverConfig: ServerConfig = {
    port: 3000,
    superSecret: "creadisecret",
    session: {
        secret: "creadi",
        name: "creadi",
        resave: false,
        saveUninitialized: false,
        proxy: false
    }
};