"use strict";
const cluster = require("cluster");
const mkdirp = require("mkdirp");
const path = require("path");
const config_1 = require("../../../config/config");
const winston_1 = require("winston");
let config = config_1.configs.getLoggingConfig();
config.file.filename = `${path.join(config.directory, "../app/logs")}/${config.file.filename}`;
if (cluster.isMaster) {
    mkdirp.sync(path.join(config.directory, "../app/logs"));
}
exports.logger = new winston_1.Logger({
    transports: [
        new winston_1.transports.File(config.file),
        new winston_1.transports.Console(config.console)
    ],
    exitOnError: false
});
exports.skip = (req, res) => {
    return res.statusCode >= 200;
};
exports.stream = {
    write: (message, encoding) => {
        exports.logger.info(message);
    }
};
