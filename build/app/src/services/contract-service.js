"use strict";
const logger_1 = require("../utils/logger");
const index_1 = require("../models/index");
class ContractService {
    createContract(contractAttributes) {
        let promise = new Promise((resolve, reject) => {
            index_1.sequelize.transaction((t) => {
                return index_1.models.Contract.create(contractAttributes).then((contract) => {
                    logger_1.logger.info(`Created contract with id ${contractAttributes.id}.`);
                    resolve(contract);
                }).catch((error) => {
                    logger_1.logger.error(error.message);
                    reject(error);
                });
            });
        });
        return promise;
    }
    retrieveContract(id) {
        let promise = new Promise((resolve, reject) => {
            index_1.sequelize.transaction((t) => {
                return index_1.models.Contract.findOne({ where: { id: id } }).then((contract) => {
                    if (contract) {
                        logger_1.logger.info(`Retrieved contract with id ${id}.`);
                    }
                    else {
                        logger_1.logger.info(`Contract with id ${id} does not exist.`);
                    }
                    resolve(contract);
                }).catch((error) => {
                    logger_1.logger.error(error.message);
                    reject(error);
                });
            });
        });
        return promise;
    }
    retrieveContracts() {
        let promise = new Promise((resolve, reject) => {
            index_1.sequelize.transaction((t) => {
                return index_1.models.Contract.findAll().then((contracts) => {
                    logger_1.logger.info("Retrieved all contracts.");
                    resolve(contracts);
                }).catch((error) => {
                    logger_1.logger.error(error.message);
                    reject(error);
                });
            });
        });
        return promise;
    }
    updateContract(id, contractAttributes) {
        let promise = new Promise((resolve, reject) => {
            index_1.sequelize.transaction((t) => {
                return index_1.models.Contract.update(contractAttributes, { where: { id: id } })
                    .then((results) => {
                    if (results.length > 0) {
                        logger_1.logger.info(`Updated contract with id ${id}.`);
                    }
                    else {
                        logger_1.logger.info(`Contract with id ${id} does not exist.`);
                    }
                    resolve(null);
                }).catch((error) => {
                    logger_1.logger.error(error.message);
                    reject(error);
                });
            });
        });
        return promise;
    }
    deleteContract(id) {
        let promise = new Promise((resolve, reject) => {
            index_1.sequelize.transaction((t) => {
                return index_1.models.Contract.destroy({ where: { id: id } }).then((afffectedRows) => {
                    if (afffectedRows > 0) {
                        logger_1.logger.info(`Deleted contract with id ${id}`);
                    }
                    else {
                        logger_1.logger.info(`Contract with id ${id} does not exist.`);
                    }
                    resolve(null);
                }).catch((error) => {
                    logger_1.logger.error(error.message);
                    reject(error);
                });
            });
        });
        return promise;
    }
}
exports.ContractService = ContractService;
exports.contractService = new ContractService();
