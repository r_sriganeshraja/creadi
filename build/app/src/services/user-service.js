"use strict";
const logger_1 = require("../utils/logger");
const index_1 = require("../models/index");
class UserService {
    createUser(userAttributes) {
        let promise = new Promise((resolve, reject) => {
            index_1.sequelize.transaction((t) => {
                return index_1.models.User.create(userAttributes).then((user) => {
                    logger_1.logger.info('Created user with name ${userAttributes.name}.');
                    resolve(user);
                }).catch((error) => {
                    if (error.name === "SequelizeUniqueConstraintError") {
                        error = new Error(`User with '${userAttributes.name}' is already exists`);
                        logger_1.logger.error('User with `${userAttributes.name}` is already exists');
                    }
                    else {
                        logger_1.logger.error(error.message);
                    }
                    reject(error);
                });
            });
        });
        return promise;
    }
    authenticateUser(email, password) {
        let promise = new Promise((resolve, reject) => {
            index_1.sequelize.transaction((t) => {
                return index_1.models.User.findOne({ where: { email: email, password: password } }).then((user) => {
                    if (user) {
                        logger_1.logger.info(`Retrieved user with EMail ID ${email}.`);
                        resolve(user);
                    }
                    else {
                        logger_1.logger.info(`User with EMail ID ${email} does not exist.`);
                        var error = new Error("Check Username/Password");
                        reject(error);
                    }
                }).catch((error) => {
                    logger_1.logger.error(error.message);
                    reject(error);
                });
            });
        });
        return promise;
    }
    retrieveUser(id) {
        let promise = new Promise((resolve, reject) => {
            index_1.sequelize.transaction((t) => {
                return index_1.models.User.findOne({ where: { id: id } }).then((user) => {
                    if (user) {
                        logger_1.logger.info(`Retrieved user with id ${id}.`);
                    }
                    else {
                        logger_1.logger.info(`User with id ${id} does not exist.`);
                    }
                    resolve(user);
                }).catch((error) => {
                    logger_1.logger.error(error.message);
                    reject(error);
                });
            });
        });
        return promise;
    }
    updateUser(id, userAttributes) {
        let promise = new Promise((resolve, reject) => {
            index_1.sequelize.transaction((t) => {
                return index_1.models.User.update(userAttributes, { where: { id: id } })
                    .then((results) => {
                    if (results.length > 0) {
                        logger_1.logger.info(`Updated user with id ${id}.`);
                    }
                    else {
                        logger_1.logger.info(`User with id ${id} does not exist.`);
                    }
                    resolve(null);
                }).catch((error) => {
                    logger_1.logger.error(error.message);
                    reject(error);
                });
            });
        });
        return promise;
    }
}
exports.UserService = UserService;
exports.userService = new UserService();
