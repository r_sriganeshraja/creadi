"use strict";
function default_1(sequelize, dataTypes) {
    let User = sequelize.define("User", {
        id: { type: dataTypes.INTEGER.UNSIGNED, allowNull: false, primaryKey: true, autoIncrement: true },
        email: { type: dataTypes.STRING, allowNull: false, unique: true },
        name: { type: dataTypes.STRING, allowNull: true },
        password: { type: dataTypes.STRING, allowNull: false }
    }, {
        indexes: [],
        classMethods: {},
        timestamps: true
    });
    return User;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
