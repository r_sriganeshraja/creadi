"use strict";
const cls = require("continuation-local-storage");
const fs = require("fs");
const path = require("path");
const SequelizeStatic = require("sequelize");
const config_1 = require("../../../config/config");
const logger_1 = require("../utils/logger");
class Database {
    constructor() {
        this._basename = path.basename(module.filename);
        let dbConfig = config_1.configs.getDatabaseConfig();
        if (dbConfig.logging) {
            dbConfig.logging = logger_1.logger.info;
        }
        SequelizeStatic.cls = cls.createNamespace("sequelize-transaction");
        this._sequelize = new SequelizeStatic(dbConfig.database, dbConfig.username, dbConfig.password, dbConfig);
        this._models = {};
        fs.readdirSync(__dirname).filter((file) => {
            return (file !== this._basename) && (file !== "interfaces");
        }).forEach((file) => {
            let model = this._sequelize.import(path.join(__dirname, file));
            this._models[model.name] = model;
        });
        Object.keys(this._models).forEach((modelName) => {
            if (typeof this._models[modelName].associate === "function") {
                this._models[modelName].associate(this._models);
            }
        });
    }
    getModels() {
        return this._models;
    }
    getSequelize() {
        return this._sequelize;
    }
}
const database = new Database();
exports.models = database.getModels();
exports.sequelize = database.getSequelize();
