"use strict";
function default_1(sequelize, dataTypes) {
    let Contract = sequelize.define("Contract", {
        id: { type: dataTypes.INTEGER.UNSIGNED, allowNull: false, primaryKey: true, autoIncrement: true },
        name: { type: dataTypes.STRING, allowNull: false, unique: true },
        description: { type: dataTypes.TEXT, allowNull: true }
    }, {
        indexes: [],
        classMethods: {},
        timestamps: true
    });
    return Contract;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
