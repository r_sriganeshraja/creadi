"use strict";
const bodyParser = require("body-parser");
const cluster = require("cluster");
const compression = require("compression");
const cookieParser = require("cookie-parser");
const express = require("express");
const http = require("http");
const morgan = require("morgan");
const os = require("os");
const path = require("path");
const config_1 = require("../../config/config");
const logger_1 = require("./utils/logger");
const auth_router_1 = require("./routers/auth-router");
const user_router_1 = require("./routers/user-router");
const contract_router_1 = require("./routers/contract-router");
const index_1 = require("./models/index");
const jwt = require("jsonwebtoken");
class Server {
    constructor() {
        this._app = express();
        this._app.use(compression());
        this._app.use(bodyParser.urlencoded({
            extended: true
        }));
        this._app.use(bodyParser.json());
        this._app.use(cookieParser());
        this._app.use(express.static(path.join(__dirname, "./public")));
        this._app.use(morgan("combined", { skip: logger_1.skip, stream: logger_1.stream }));
        this._app.use((error, req, res, next) => {
            if (error) {
                res.status(400).send(error);
            }
        });
        this._app.use("/auth", auth_router_1.router);
        this._app.use((req, res, next) => {
            var token = req.body.token || req.params.token || req.headers['x-access-token'];
            if (!token && req.headers["authorization"]) {
                token = req.headers["authorization"].split(" ")[1];
            }
            if (token) {
                jwt.verify(token, config_1.configs.getServerConfig().superSecret, (err, decoded) => {
                    if (err) {
                        return res.json({ success: false, message: 'Failed to authenticate token.' });
                    }
                    else {
                        next();
                    }
                });
            }
            else {
                return res.status(403).send({
                    success: false,
                    message: 'No token provided.'
                });
            }
        });
        this._app.use("/user", user_router_1.router);
        this._app.use("/contract", contract_router_1.router);
        this._server = http.createServer(this._app);
    }
    _onError(error) {
        if (error.syscall) {
            throw error;
        }
        let port = config_1.configs.getServerConfig().port;
        let bind = `Port ${port}`;
        switch (error.code) {
            case "EACCES":
                logger_1.logger.error(`[EACCES] ${bind} requires elevated privileges.`);
                process.exit(1);
                break;
            case "EADDRINUSE":
                logger_1.logger.error(`[EADDRINUSE] ${bind} is already in use.`);
                process.exit(1);
                break;
            default:
                throw error;
        }
    }
    ;
    _onListening() {
        let address = this._server.address();
        let bind = `port ${address.port}`;
        logger_1.logger.info(`Listening on ${bind}.`);
    }
    ;
    start() {
        if (cluster.isMaster) {
            index_1.sequelize.sync().then(() => {
                logger_1.logger.info("Database synced.");
                for (let c = 0; c < os.cpus().length; c++) {
                    cluster.fork();
                }
                cluster.on("exit", (worker, code, signal) => {
                    logger_1.logger.info(`Worker ${worker.process.pid} died.`);
                });
                cluster.on("listening", (worker, address) => {
                    logger_1.logger.info(`Worker ${worker.process.pid} connected to port ${address.port}.`);
                });
            }).catch((error) => {
                logger_1.logger.error(error.message);
            });
        }
        else {
            this._server.listen(3000);
            this._server.on("error", error => this._onError(error));
            this._server.on("listening", () => this._onListening());
        }
    }
    stop() {
        this._server.close();
        process.exit(0);
    }
}
let server = new Server();
server.start();
process.on("SIGINT", () => {
    server.stop();
});
