"use strict";
const user_service_1 = require("../services/user-service");
const express_1 = require("express");
const config_1 = require("../../../config/config");
const jwt = require("jsonwebtoken");
exports.router = express_1.Router();
exports.router.post("/register", (req, res) => {
    user_service_1.userService.createUser(req.body).then((user) => {
        return res.status(201).send(user);
    }).catch((error) => {
        return res.status(409).send(error);
    });
});
exports.router.post("/authenticate", (req, res) => {
    user_service_1.userService.authenticateUser(req.body.email, req.body.password).then((user) => {
        var token = jwt.sign({ 'email': req.body.email }, config_1.configs.getServerConfig().superSecret, {
            expiresIn: 86400
        });
        return res.json({
            success: true,
            message: 'Enjoy your token!',
            token: token
        });
    }).catch((error) => {
        return res.status(401).json({
            success: false,
            message: error.message,
            token: null
        });
    });
});
