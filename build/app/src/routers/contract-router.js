"use strict";
const contract_service_1 = require("../services/contract-service");
const express_1 = require("express");
exports.router = express_1.Router();
exports.router.post("/", (req, res) => {
    contract_service_1.contractService.createContract(req.body).then((contract) => {
        return res.status(201).send(contract);
    }).catch((error) => {
        return res.status(409).send(error);
    });
});
exports.router.get("/:id", (req, res) => {
    contract_service_1.contractService.retrieveContract(req.params.id).then((contract) => {
        if (contract) {
            return res.send(contract);
        }
        else {
            return res.sendStatus(404);
        }
    }).catch((error) => {
        return res.status(500).send(error);
    });
});
exports.router.get("/", (req, res) => {
    contract_service_1.contractService.retrieveContracts().then((contracts) => {
        return res.send(contracts);
    }).catch((error) => {
        return res.status(500).send(error);
    });
});
exports.router.post("/:id", (req, res) => {
    contract_service_1.contractService.updateContract(req.params.id, req.body).then(() => {
        return res.sendStatus(200);
    }).catch((error) => {
        return res.status(409).send(error);
    });
});
exports.router.delete("/:id", (req, res) => {
    contract_service_1.contractService.deleteContract(req.params.id).then(() => {
        return res.sendStatus(200);
    }).catch((error) => {
        return res.status(500).send(error);
    });
});
