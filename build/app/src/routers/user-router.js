"use strict";
const user_service_1 = require("../services/user-service");
const express_1 = require("express");
exports.router = express_1.Router();
exports.router.get("/:id", (req, res) => {
    user_service_1.userService.retrieveUser(req.params.id).then((user) => {
        if (user) {
            return res.send(user);
        }
        else {
            return res.sendStatus(404);
        }
    }).catch((error) => {
        return res.status(500).send(error);
    });
});
exports.router.post("/:id", (req, res) => {
    user_service_1.userService.updateUser(req.params.id, req.body).then(() => {
        return res.sendStatus(200);
    }).catch((error) => {
        return res.status(409).send(error);
    });
});
