"use strict";
const chai_1 = require("chai");
const index_1 = require("../../src/models/index");
const delay = 500;
describe("ContractService", () => {
    const contractAttributes = {
        id: 101,
        name: "Contract 101",
        description: "Contract 101 Full Description"
    };
    const updateContractAttributes = {
        name: "Contract 102",
        description: "Contract 102 Full Description"
    };
    describe("#Contract Actions", () => {
        let contractService;
        before((done) => {
            setTimeout(() => {
                index_1.sequelize.sync().then(() => {
                    let service = require("../../src/services/contract-service");
                    contractService = service.contractService;
                    done();
                }).catch((error) => {
                    done(error);
                });
            }, delay);
        });
        it("should create a contract in the database correctly", () => {
            contractService.createContract(contractAttributes).then((contract) => {
                chai_1.expect(contract.dataValues.name).to.equals(contractAttributes.name);
                chai_1.expect(contract.dataValues.description).to.equals(contractAttributes.description);
            }).catch((error) => {
                console.log(error.message);
            });
        });
        it("should retrieve a contract from the database correctly", () => {
            contractService.retrieveContract(contractAttributes.id).then((contract) => {
                chai_1.expect(contract.dataValues.name).to.equals(contractAttributes.name);
                chai_1.expect(contract.dataValues.description).to.equals(contractAttributes.description);
            }).catch((error) => {
                console.log(error.message);
            });
        });
        it("should update a contract from the database correctly", () => {
            contractService.updateContract(contractAttributes.id, updateContractAttributes).then((result) => {
                chai_1.expect(result).to.be.a('null');
            }).catch((error) => {
                console.log(error.message);
            });
        });
        it("should delete a contract from the database correctly", () => {
            contractService.deleteContract(contractAttributes.id).then((result) => {
                chai_1.expect(result).to.be.a('null');
            }).catch((error) => {
                console.log(error.message);
            });
        });
    });
});
