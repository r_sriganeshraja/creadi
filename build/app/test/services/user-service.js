"use strict";
const chai_1 = require("chai");
const index_1 = require("../../src/models/index");
const delay = 500;
describe("UserService", () => {
    const newUserAttributes = {
        id: 101,
        name: "User 1",
        email: "email1@email.com",
        password: "password123456"
    };
    const updateUserAttributes = {
        name: "User 2",
        email: "email2@email.com",
        password: "password123456"
    };
    describe("#User Actions", () => {
        let userService;
        before((done) => {
            setTimeout(() => {
                index_1.sequelize.sync().then(() => {
                    let service = require("../../src/services/user-service");
                    userService = service.userService;
                    done();
                }).catch((error) => {
                    done(error);
                });
            }, delay);
        });
        it("should create a user in the database correctly", () => {
            userService.createUser(newUserAttributes).then((user) => {
                chai_1.expect(user.dataValues.name).to.equals(newUserAttributes.name);
                chai_1.expect(user.dataValues.email).to.equals(newUserAttributes.email);
                chai_1.expect(user.dataValues.password).to.equals(newUserAttributes.password);
            }).catch((error) => {
                console.log(error.message);
            });
        });
        it("should retrieve a user from the database correctly", () => {
            userService.retrieveUser(newUserAttributes.id).then((user) => {
                chai_1.expect(user.dataValues.email).to.equals(newUserAttributes.email);
                chai_1.expect(user.dataValues.name).to.equals(newUserAttributes.name);
            }).catch((error) => {
                console.log(error.message);
            });
        });
        it("should update a user from the database correctly", () => {
            userService.updateUser(newUserAttributes.id, updateUserAttributes).then((result) => {
                chai_1.expect(result).to.be.a('null');
            }).catch((error) => {
                console.log(error.message);
            });
        });
    });
});
