"use strict";
exports.databaseConfig = {
    username: "creadi",
    password: "creadi",
    database: "creadi",
    host: "127.0.0.1",
    port: 3306,
    dialect: "mysql",
    logging: true,
    force: false,
    timezone: "+01:00"
};
