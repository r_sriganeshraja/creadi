"use strict";
exports.loggingConfig = {
    file: {
        level: "info, error",
        filename: "creadi.log",
        handleExceptions: true,
        json: true,
        maxsize: 5242880,
        maxFiles: 100,
        colorize: false
    },
    console: {
        level: "info, error",
        handleExceptions: true,
        json: false,
        colorize: true
    },
    directory: __dirname
};
