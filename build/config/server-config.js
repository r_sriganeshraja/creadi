"use strict";
exports.serverConfig = {
    port: 3000,
    superSecret: "creadisecret",
    session: {
        secret: "creadi",
        name: "creadi",
        resave: false,
        saveUninitialized: false,
        proxy: false
    }
};
